export const environment = {
  production: true,
  authConfig: {
    clientID: 'aL4iVcoEeBk1WsVZZ8LtusTaV36VTpL4',
    domain: 'irina-auth.auth0.com',
    callbackURL: 'http://localhost:4000/callback'
  }
};
