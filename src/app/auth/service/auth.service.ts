import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Auth0Callback, AuthOptions, CheckSessionOptions, WebAuth} from 'auth0-js';
import {environment} from '../../../environments/environment';
import {AuthData} from '../models/auth-data.model';

@Injectable()
export class AuthService  {

  private _auth0: WebAuth;
  private _properties: AuthOptions;

  private _authData: AuthData;
  private _authConfig = environment.authConfig;

  constructor(public router: Router) {
    this._properties = {
      clientID: this._authConfig.clientID,
      domain: this._authConfig.domain,
      responseType: 'token id_token',
      redirectUri: this._authConfig.callbackURL,
      scope: 'openid profile'
    };
    this._auth0 = new WebAuth({...this._properties});
  }

  public login(): void {
    this._auth0.authorize();
  }

  /* Ищет результат аутентификации в хэше URL.
   Затем результат обрабатывается parseHash методом из auth0.js.*/
  public handleAuthentication(): Observable<boolean> {
    return Observable.create(observer => {
      this._auth0.parseHash((err, authResult) => {
        if (authResult && authResult.accessToken && authResult.idToken) {
          window.location.hash = '';
          this._setSession(authResult);
          observer.next(true);
          observer.complete();
          this.router.navigate(['/']);
        } else if (err) {
          observer.error(err);
          observer.complete();
        }
      });
    });
  }

  private _setSession(authResult): void {
    const expiresAt = (authResult.expiresIn * 1000) + new Date().getTime();
    this._authData = {
      expiresAt: expiresAt,
      accessToken: authResult.accessToken,
      idToken: authResult.idToken
    };
  }

  // public renewTokens(): void {
  //   const options: CheckSessionOptions = {
  //     responseType: 'token id_token',
  //     scope: 'openid profile',
  //     redirectUri: this._authConfig.callbackURL
  //   };
  //
  //   const func: Auth0Callback<any> = (err, authResult) => {
  //     if (authResult && authResult.accessToken && authResult.idToken) {
  //       this._setSession(authResult);
  //     } else if (err) {
  //       this.logout();
  //       throw new Error(`Could not get a new token (${err.error}: ${err.error}).`);
  //     }
  //   };
  //   this._auth0.checkSession(options, func);
  // }

  public logout(): void {
    this._authData = null;
    this.router.navigate(['/']);
  }

  public isAuthenticated(): boolean {
    return this._authData && this._authData.expiresAt ? new Date().getTime() < this._authData.expiresAt : false;
  }

  public getProfile(): Observable<any> {
    return Observable.create(observer => {
      this._auth0.client.userInfo(this._authData.accessToken, (err, profile) => {
        if (err) {
          observer.error(err);
          observer.complete();
        } else if (profile) {
          observer.next(profile);
          observer.complete();
        }
      });
    });
  }
}
