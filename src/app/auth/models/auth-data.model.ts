export class AuthData {
  accessToken?: string;
  idToken?: string;
  expiresAt?: number;
}

// export class AuthData {
//   get accessToken(): string {
//     return this._authData.accessToken ? this._authData.accessToken : this._authData.accessToken = this.get().accessToken;
//   }
//
//   get idToken(): string {
//     return this._authData.idToken ? this._authData.idToken : this._authData.idToken = this.get().idToken;
//   }
//
//   get expiresAt(): number {
//     return this._authData.expiresAt ? this._authData.expiresAt : this._authData.expiresAt = +this.get().expiresAt;
//   }
//
//   constructor(private _authData: IAuthData) {
//     if (Object.keys(_authData).length !== 0) {
//       localStorage.setItem('auth', JSON.stringify(this._authData));
//     }
//   }
//
//   private get(): IAuthData {
//     return localStorage.getItem('auth') ? JSON.parse(localStorage.getItem('auth')) : {};
//   }
//
//   public delete() {
//     localStorage.removeItem('auth');
//   }
// }
