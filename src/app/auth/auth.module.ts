
import {NgModule} from '@angular/core';
import {AuthService} from './service/auth.service';
import {CommonModule} from '@angular/common';
import {AuthInterceptor} from './service/auth.interceptor';
import {HTTP_INTERCEPTORS} from '@angular/common/http';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    }
  ]
})
export class AuthModule {
}
