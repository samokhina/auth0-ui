import {Component, OnInit} from '@angular/core';
import {AuthService} from './auth/service/auth.service';
import {catchError, flatMap} from 'rxjs/operators';
import {Observable} from 'rxjs/Observable';
import {ProfileService} from './profile/service/profile.service';
import {Profile} from './profile/model/profile.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  userProfile: any = null;

  constructor(private authService: AuthService,
              private profileService: ProfileService) {
  }

  ngOnInit() {
    if (!this.isAuthenticated()) {
      const getProfile$ = this.authService.handleAuthentication()
        .pipe(
          flatMap(() => this.profileService.getProfile()),
          catchError(err => {
            return Observable.throw(err);
          })
        );
      getProfile$
        .subscribe((profile: Profile) => this.userProfile = profile);
    } else {
      this.profileService.getProfile()
        .subscribe((profile: Profile) => this.userProfile = profile);
    }
  }

  login() {
    this.authService.login();
  }

  isAuthenticated() {
    return this.authService.isAuthenticated();
  }

  logout() {
    return this.authService.logout();
  }
}
