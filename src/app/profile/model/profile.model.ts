export class Profile {
  name: string;
  family: string;
  fio: string;
  nickname: string;
  picture: string;
}
