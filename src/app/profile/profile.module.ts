
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProfileService} from './service/profile.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    ProfileService
  ]
})
export class ProfileModule {
}
