import {Injectable} from '@angular/core';
import {AuthService} from '../../auth/service/auth.service';
import {Observable} from 'rxjs/Observable';
import {Profile} from '../model/profile.model';
import 'rxjs/add/operator/map';
import {of} from 'rxjs/observable/of';


@Injectable()
export class ProfileService {

  private _userProfile: Profile = null;

  constructor(public authService: AuthService) {}

  public getProfile(): Observable<Profile> {
    if (this._userProfile) {
      return of(this._userProfile);
    } else {
      return this.authService.getProfile()
        .map((profile) => {
          let authUser;
          authUser = new Profile();
          authUser.name = profile.given_name;
          authUser.family = profile.family_name;
          authUser.fio = profile.name;
          authUser.nickname = profile.nickname;
          authUser.picture = profile.picture;
          this._userProfile = authUser;
          return authUser;
        });
    }
  }


  // TODO вызывать в гварде
  // public isAdmin(): boolean {
  //   if (this._accessToken) {
  //     const helper = new JwtHelperService();
  //     const decodedToken = helper.decodeToken(this._accessToken);
  //     if (decodedToken['http://localhost:3000/roles'].indexOf('admin') > -1) {
  //       return true;
  //     } else {
  //       return false;
  //     }
  //   } else {
  //     return false;
  //   }
  // }
}
