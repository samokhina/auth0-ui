import { Component, OnInit } from '@angular/core';
import {Item} from './item.interface';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../auth/service/auth.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  items: Item [] = [{
    name: 'Pizza',
    price: 3
  },
    {
      name: 'Salad',
      price: 2
    }];

  itemSubmitted = false;
  itemForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService) { }

  ngOnInit() {
    this.itemForm = this.formBuilder.group({
      name: ['', Validators.required],
      price: ['', [Validators.required, Validators.min(0)]]
    });
  }

  get getItemForm() {
    return this.itemForm.controls;
  }

  addNewItem() {
    this.itemSubmitted = true;
    if (this.itemForm.invalid) {
      console.log(this.itemForm);
    } else {
      this.items.push(this.itemForm.value);
    }
  }

  addToCart() {
    window.alert('Added');
  }
}
