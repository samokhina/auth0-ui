import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from '../auth/service/auth.service';
import {Item} from './item.interface';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ItemsService {

  constructor(private _http: HttpClient,
              private _authService: AuthService) {
  }

  // creates header
  // private _authHeader(): Object {
  //   return {
  //     headers: new HttpHeaders({'authorization': `Bearer ${this._authService.getAccessToken()}`})
  //   };
  // }
  //
  // public getItems(): Observable<Item[]> {
  //   return this._http.get<Item[]>('/api/items');
  // }
  //
  // public postItems(item: Item): Observable<Item> {
  //   return this._http.post<Item>('/api/items', item, this._authHeader());
  // }
  //
  // public postToShoppingCart(): Observable<String> {
  //   return this._http.post<String>('/api/shopping-cart', '', this._authHeader());
  // }
}
